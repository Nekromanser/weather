Thank you very much for your support request! We have registered your request as ticket %{ISSUE_ID} and will answer as soon as possible.
***
Vielen Dank für Ihre Support-Anfrage! Wir haben Ihre Anfrage als Ticket %{ISSUE_ID} registriert und werden Ihnen so schnell wie möglich antworten.
