# Weather

<img src="https://gitlab.com/BeowuIf/wetter/-/raw/master/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png" height=75px> **Weather App with data from OpenWeatherMap**

## Features

- Current weather
- 7 day forecast
- 48 hour forecast
- Changing the city
- Multiple cities
- Multiple units
- smaller than 2MB

## Supported languages
- English
- German
- Portuguese


## Get an API-Key

Go to [OpenWeatherMap](https://home.openweathermap.org/api_keys) and sign up for free. It may take a while before the API-Key is activated!


## Contributing

See our [Contributing doc](CONTRIBUTING.md) for information on how to report
issues or translate the app into your language.


## Licensing

See the [LICENSE](LICENSE) file for licensing information as it pertains to files in this repository.

## Wiki

You have questions, need help to set up the app or simply want to know more about it? Then take a look at the [Wiki](https://gitlab.com/BeowuIf/weather/-/wikis/home).
