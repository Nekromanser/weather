package de.beowulf.wetter

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import de.beowulf.wetter.databinding.ActivityStartBinding
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import java.util.concurrent.Executors

class StartActivity : AppCompatActivity() {

    private lateinit var binding: ActivityStartBinding

    private val gf = GlobalFunctions()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gf.initializeContext(this)

        val settings: SharedPreferences = getSharedPreferences("de.beowulf.wetter", 0)

        binding.CreatedBy.movementMethod = LinkMovementMethod.getInstance()
        binding.ReportError.movementMethod = LinkMovementMethod.getInstance()
        binding.OWM.movementMethod = LinkMovementMethod.getInstance()

        var lat: String = settings.getString("lat", "")!!
        var city: String = settings.getString("city", "")!!
        var unit: Int = settings.getInt("unit", 0)
        var api: String = settings.getString("api", "")!!
        val change: Boolean = intent.getBooleanExtra("change", false)

        val units = resources.getStringArray(R.array.units)
        binding.Unit.adapter = ArrayAdapter(this, R.layout.spinner, units)

        if (change) {
            binding.City.setText(city)
            binding.Unit.setSelection(unit)
            if (api != getString(R.string.standardKey))
                binding.Api.setText(api)
        }

        binding.ApiText.setOnClickListener {
            val uriUrl: Uri = Uri.parse("https://home.openweathermap.org/api_keys")
            val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
            startActivity(launchBrowser)
        }

        if (lat != "" && !change) { // When already initialized load data and start the MainActivity
            binding.loader.visibility = View.VISIBLE
            binding.mainContainer.visibility = View.GONE

            startMain(false, api)

        } else binding.Submit.setOnClickListener { // When not already initialized, set Submit-Button OnClickListener and check the data
            binding.loader.visibility = View.VISIBLE
            binding.mainContainer.visibility = View.GONE
            val executor = Executors.newScheduledThreadPool(5)

            doAsync(executorService = executor) {
                var error = 0
                val result: String? = try {
                    city = binding.City.text.toString()
                    api = binding.Api.text.toString()
                    if (api == "")
                        api = getString(R.string.standardKey)
                    URL("https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$api").readText(Charsets.UTF_8)
                } catch (e: Exception) {
                    try {
                        with(URL("https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$api").openConnection() as HttpURLConnection) {
                            requestMethod = "GET"
                            error = if (responseCode == 429 || responseCode == 401) {
                                1
                            } else {
                                2
                            }
                        }
                    } catch (e: Exception) {
                        error = 3
                    }
                    null
                }
                uiThread {
                    if (result != null) {
                        val jsonObj = JSONObject(result)
                        val coord: JSONObject = jsonObj.getJSONObject("coord")
                        val sys: JSONObject = jsonObj.getJSONObject("sys")

                        val lon: String = coord.getString("lon")
                        lat = coord.getString("lat")
                        city = jsonObj.getString("name") + ", " + sys.getString("country")
                        unit = binding.Unit.selectedItemPosition

                        // Save all needed values
                        val editor: SharedPreferences.Editor = settings.edit()
                        editor.putString("lon", lon)
                        editor.putString("lat", lat)
                        editor.putString("city", city)
                        editor.putInt("unit", unit)
                        editor.putString("api", api)
                        editor.apply()

                        startMain(true, api)
                    } else {
                        binding.loader.visibility = View.GONE
                        binding.mainContainer.visibility = View.VISIBLE
                        when (error) {
                            1 -> {
                                Toast.makeText(this@StartActivity, R.string.error_1_start, Toast.LENGTH_SHORT).show()
                            }
                            2 -> {
                                Toast.makeText(this@StartActivity,  R.string.error_2_start, Toast.LENGTH_SHORT).show()
                            }
                            else -> {
                                Toast.makeText(this@StartActivity,  R.string.error_3_start, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun startMain(mustSuccess: Boolean, api: String) {
        // Message when you use standard API-Key
        if (api == getString(R.string.standardKey)) {
            Toast.makeText(this@StartActivity, getString(R.string.ErrorStandardAPI), Toast.LENGTH_LONG).show()
        }

        // Load new data
        val executor = Executors.newScheduledThreadPool(5)

        doAsync(executorService = executor) {
            val result: String? = try {
                URL(gf.url("normal", "")).readText(Charsets.UTF_8)
            } catch (e: Exception) {
                null
            }
            uiThread {
                if (result != null || !mustSuccess) { // when loaded data isn't empty, or old data is available, start the Main activity
                    if (result != null) {
                        gf.setResult(result)
                        gf.setInitialized(true)
                    } else {
                        Toast.makeText(this@StartActivity, R.string.error_occurred, Toast.LENGTH_LONG).show()
                    }
                    val intent = Intent(this@StartActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else { // else display error message
                    Toast.makeText(this@StartActivity, R.string.error_3_start, Toast.LENGTH_LONG).show()
                    binding.loader.visibility = View.GONE
                    binding.mainContainer.visibility = View.VISIBLE
                }
            }
        }
    }
}