package de.beowulf.wetter

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.LinearLayout
import de.beowulf.wetter.adapter.MyListHourAdapter
import de.beowulf.wetter.databinding.WeatherForecastBinding
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class HourFragment : Fragment() {

    private lateinit var binding: WeatherForecastBinding

    private val hourTime = arrayOfNulls<String>(48)
    private val statusHour = arrayOfNulls<Int>(48)
    private val statusHourText = arrayOfNulls<String>(48)
    private val temp = arrayOfNulls<String>(48)
    private val wind = arrayOfNulls<String>(48)
    private val pressure = arrayOfNulls<String>(48)
    private val humidity = arrayOfNulls<String>(48)
    private val cloudiness = arrayOfNulls<String>(48)
    private val visibility = arrayOfNulls<String>(48)
    private val rainSnow = arrayOfNulls<String>(48)
    private val rainSnowType = arrayOfNulls<String>(48)
    private val gf = GlobalFunctions()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = WeatherForecastBinding.inflate(layoutInflater)
        val view: View = binding.root

        gf.initializeContext(context!!)

        val jsonObj = gf.result()

        binding.ListView.onItemClickListener =
            AdapterView.OnItemClickListener { _, v, _, _ ->
                val moreInfo = v.findViewById<LinearLayout>(R.id.hourMoreInfo)
                if (moreInfo.visibility == View.GONE) {
                    moreInfo.visibility = View.VISIBLE
                } else {
                    moreInfo.visibility = View.GONE
                }
            }

        for (i: Int in 0..47) {
            val hourly: JSONObject = jsonObj.getJSONArray("hourly").getJSONObject(i)
            val hourlyWeather: JSONObject = hourly.getJSONArray("weather").getJSONObject(0)

            val icon: Int = gf.icon(hourlyWeather.getString("icon"))

            /* Populating extracted data into our views */
            hourTime[i] = SimpleDateFormat(getString(R.string.daytime), Locale.ROOT).format(
                Date(
                    hourly.getLong("dt") * 1000
                )
            )
            var tempCut: String = hourly.getString("temp")
            tempCut = tempCut.split(".")[0]
            val windDouble: Double = hourly.getDouble("wind_speed")
            val windDegree: Int = hourly.getInt("wind_deg")
            statusHour[i] = icon
            statusHourText[i] = hourlyWeather.getString("description")
            temp[i] = tempCut + gf.unitTemp()
            wind[i] = gf.changeDecimalSeparator(windDouble) + gf.unitSpeed() + " (${gf.degToCompass(windDegree)})"
            pressure[i] = hourly.getString("pressure") + "hPa"
            humidity[i] = hourly.getString("humidity") + "%"
            cloudiness[i] = hourly.getString("clouds") + "%"
            val visibilityDouble: Double = hourly.getDouble("visibility") / 1000
            visibility[i] = gf.changeDecimalSeparator(visibilityDouble) + "km"
            val precipitation: Double = hourly.getDouble("pop") * 100
            when {
                hourly.has("rain") -> {
                    val rainDouble: Double = hourly.getJSONObject("rain").getDouble("1h")
                    rainSnow[i] = gf.changeDecimalSeparator(rainDouble)
                    rainSnowType[i] = "rain"
                }
                hourly.has("snow") -> {
                    val snowDouble: Double = hourly.getJSONObject("snow").getDouble("1h")
                    rainSnow[i] = gf.changeDecimalSeparator(snowDouble)
                    rainSnowType[i] = "snow"
                }
                else -> {
                    rainSnow[i] = "0"
                    rainSnowType[i] = "rain"
                }
            }
            rainSnow[i] = rainSnow[i] + "mm (${precipitation.toString().split(".")[0]}%)"
        }

        val myListAdapter = MyListHourAdapter(
            activity!!,
            hourTime,
            statusHour,
            statusHourText,
            temp,
            wind,
            pressure,
            humidity,
            cloudiness,
            visibility,
            rainSnow,
            rainSnowType
        )
        binding.ListView.adapter = myListAdapter

        return view
    }
}