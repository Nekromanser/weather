package de.beowulf.wetter

import android.content.Context
import android.content.SharedPreferences
import org.json.JSONObject
import java.text.DecimalFormatSymbols
import java.util.*
import kotlin.math.floor

class GlobalFunctions {

    private lateinit var settings: SharedPreferences
    private lateinit var context: Context

    fun initializeContext(con: Context) {
        settings = con.getSharedPreferences("de.beowulf.wetter", 0)
        context = con
    }

    fun changeDecimalSeparator(doubleValue: Double): String {
        val separator: Char = DecimalFormatSymbols.getInstance().decimalSeparator
        return doubleValue.toString().replace('.', separator)
    }

    fun degToCompass(num: Int): String {
        val arr: Array<String> = context.resources.getStringArray(R.array.Degree)
        val value: Int = floor((num / 22.5) + 0.5).toInt()
        return arr[(value % 16)]
    }

    fun result(): JSONObject {
        return JSONObject(settings.getString("result", "").toString())
    }

    fun setResult(result: String) {
        val editor: SharedPreferences.Editor = settings.edit()
        editor.putString("result", result)
        editor.putBoolean("initialized", true)
        editor.apply()
    }

    fun url(type: String, city: String): String {
        val api = settings.getString("api", "")
        val langCode: Array<String> = context.resources.getStringArray(R.array.lang_Code)

        var lang = "en"
        if (langCode.indexOf(Locale.getDefault().language) != -1) {
            lang = Locale.getDefault().language
        }
        val unitName =
            context.resources.getStringArray(R.array.unitsName)[settings.getInt("unit", 0)]

        return if (type == "cities") {
            "https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$api&units=$unitName&lang=$lang"
        } else {
            val lat = settings.getString("lat", "")!!
            val lon = settings.getString("lon", "")!!
            "https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$lon&exclude=minutely,alerts&units=$unitName&appid=$api&lang=$lang"
        }
    }

    fun unitTemp(): String {
        return context.resources.getStringArray(R.array.unitsTemp)[settings.getInt("unit", 0)]
    }

    fun unitSpeed(): String {
        return context.resources.getStringArray(R.array.unitsSpeed)[settings.getInt("unit", 0)]
    }

    fun icon(iconName: String): Int {
        val icon =
            if (!iconName.startsWith("01") && !iconName.startsWith("02") && !iconName.startsWith("10")) {
                "status${iconName.dropLast(1)}"
            } else {
                "status$iconName"
            }
        return context.resources.getIdentifier(icon, "drawable", context.packageName)
    }

    fun getCities(): Array<String> {
        return settings.getStringSet("cities", emptySet())?.toTypedArray() ?: emptyArray()
    }

    fun setCities(cities: Array<String>) {
        val editor: SharedPreferences.Editor = settings.edit()
        editor.putStringSet("cities", cities.toSet())
        editor.apply()
    }

    fun getInitialized(): Boolean {
        return settings.getBoolean("initialized", false)
    }

    fun setInitialized(bool: Boolean) {
        settings.edit()
            .putBoolean("initialized", bool)
            .apply()
    }
}