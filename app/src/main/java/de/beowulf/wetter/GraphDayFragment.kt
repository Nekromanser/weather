package de.beowulf.wetter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.beowulf.wetter.adapter.DataPoint
import de.beowulf.wetter.databinding.FragmentGraphBinding

class GraphDayFragment : Fragment() {

    private lateinit var binding: FragmentGraphBinding

    private val temp = arrayOfNulls<Int>(8)
    private val precipitation = arrayOfNulls<Int>(8)
    private val gf = GlobalFunctions()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGraphBinding.inflate(layoutInflater)
        val view: View = binding.root

        gf.initializeContext(context!!)

        binding.graphView.setData(temp())
        binding.graphView2.setData(precipitation())

        return view
    }

    private fun temp(): List<DataPoint> {
        val jsonObj = gf.result()

        for (i: Int in 0..7) {
            temp[i] = (jsonObj.getJSONArray("daily").getJSONObject(i).getJSONObject("temp")
                .getInt("max") + jsonObj.getJSONArray("daily").getJSONObject(i)
                .getJSONObject("temp").getInt("min")) / 2
        }
        return (0..7).map {
            DataPoint(it, temp[it]!!)
        }
    }

    private fun precipitation(): List<DataPoint> {
        val jsonObj = gf.result()

        for (i: Int in 0..7) {
            precipitation[i] =
                (jsonObj.getJSONArray("daily").getJSONObject(i).getDouble("pop") * 100).toInt()
        }
        return (0..7).map {
            DataPoint(it, precipitation[it]!!)
        }
    }
}